package com.checkpoint.jms.listener.test;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.checkpoint.jms.listener.ConsumerListener;

public class ConsumerListenerTest {

	private TextMessage message;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOnMessage() {
		ConsumerListener cl = new ConsumerListener();
		cl.onMessage(message);
		assertNull(message);
	}

}
